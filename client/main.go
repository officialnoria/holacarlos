package main

import (
	"context"
	"holaCarlos/proto"
	"log"
	"os"
	"time"
	"google.golang.org/grpc"
)

func main() {
	conn, err := grpc.Dial("localhost:4040", grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		panic(err)
	}

	defer conn.Close()
	c := proto.NewAddServiceClient(conn)

	name := "mundo"
	if len(os.Args) > 1 {
		name = os.Args[1]
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Greet(ctx, &proto.Request{N: name})
	if err != nil {
		log.Fatalf("no se pudo saludar: %v", err)
	}
	log.Printf("%s", r.GetResult())
}